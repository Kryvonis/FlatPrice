import os
import logging
import pickle
from logging.handlers import RotatingFileHandler

import redis
from celery import Celery
from flask import Flask
import pandas as pd
import numpy as np

from app.settings import config


def create_app(config_name='default'):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    formatter = logging.Formatter(
        '[%(asctime)s] - %(levelname)s - %(message)s')

    if not os.path.exists(os.path.join(app.config['APP_DIR'], 'var', 'log')):
        os.makedirs(os.path.join(app.config['APP_DIR'], 'var', 'log'))
    error_handler = RotatingFileHandler(
        os.path.join(app.config['APP_DIR'], 'var', 'log', 'error.log'),
        maxBytes=10000, backupCount=1)
    error_handler.setLevel(logging.ERROR)
    error_handler.setFormatter(formatter)

    info_handler = RotatingFileHandler(
        os.path.join(app.config['APP_DIR'], 'var', 'log', 'info.log'),
        maxBytes=10000, backupCount=1)
    info_handler.setLevel(logging.INFO)
    info_handler.setFormatter(formatter)

    app.logger.addHandler(info_handler)

    return app


def create_df():
    df = pd.read_csv('parse_result.csv')
    df['date'] = pd.to_datetime(df['date'])
    return df


app = create_app()




xgb_model = pickle.load(open(app.config['MODEL_FILE'], "rb"))

df = pd.read_csv(app.config['DATA_FILE'])

df['date'] = pd.to_datetime(df['date'])

mean_square_disctricts = df.groupby('district')['square'].apply(
    lambda x: np.mean(x)).to_dict()
median_square_disctricts = df.groupby('district')['square'].apply(
    lambda x: np.median(x)).to_dict()

mean_rooms_disctricts = df.groupby('district')['rooms'].apply(
    lambda x: np.mean(x)).to_dict()
median_rooms_disctricts = df.groupby('district')['rooms'].apply(
    lambda x: np.median(x)).to_dict()

avg_price_per_room_disctricts = df.groupby('district')['price/rooms'].apply(
    lambda x: np.mean(x)).to_dict()
avg_price_per_square_disctricts = df.groupby('district')['price/square'].apply(
    lambda x: np.mean(x)).to_dict()



districts_dict = {
        'district_Голосіївський': 0,
        'district_Дарницький': 0,
        'district_Деснянський': 0,
        'district_Дніпровський': 0,
        'district_Оболонський': 0,
        'district_Печерський': 0,
        'district_Подільський': 0,
        'district_Святошинський': 0,
        "district_Солом'янський": 0,
        'district_Шевченківський': 0
    }

districts = ['district_Голосіївський',
                 'district_Дарницький', 'district_Деснянський',
                 'district_Дніпровський',
                 'district_Оболонський', 'district_Печерський',
                 'district_Подільський',
                 'district_Святошинський', "district_Солом'янський",
                 'district_Шевченківський']

from app.apis import api

api.init_app(app)
