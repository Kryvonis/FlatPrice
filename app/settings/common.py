import os
import json


class Config(object):
    # Flask settings
    CONFIG_NAME = 'default'

    DEBUG = False
    TESTING = False
    APP_DIR = os.path.dirname(
        os.path.dirname(
            os.path.dirname(os.path.abspath(__file__))
        )
    )

    SECRET = 'Super_secret'
    HOST = os.getenv('HOST', '127.0.0.1')
    PORT = int(os.getenv('PORT', 5000))

    # ES configs
    ES_HOST = ''
    ES_PORT = 9200
    ES_USER = 'admin'
    ES_PASS = ''

    ES_SOLUTION_INDEX = 'solutions'
    ES_QUESTION_INDEX = 'questions'

    ES_INTENTS_INDEX = ''
    ES_TEXT_TYPE = ''
    ES_INTENT_TYPE = ''

    # Google configs
    GOOGLE_API_KEY = os.getenv('GOOGLE_API_KEY', '')

    DATA_DIR = os.path.join(APP_DIR, 'data')
    MODEL_DIR = os.path.join(DATA_DIR, 'models')
    MODEL_EXT = "pkl"


    @staticmethod
    def init_app(app):
        pass


class ProductionConfig(Config):
    # Config settings
    CONFIG_NAME = 'production'
    pass


class DevelopmentConfig(Config):
    # Config settings
    CONFIG_NAME = 'develop'
    DEBUG = True


class TestingConfig(Config):
    # Config settings
    CONFIG_NAME = 'testing'
    TESTING = True
