try:
    from app.settings.local import (DevelopmentConfig, TestingConfig,
                                    ProductionConfig)
except ImportError:
    from app.settings.common import (DevelopmentConfig, TestingConfig,
                                     ProductionConfig)

config = {
    'develop': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}

project_config = config['develop']