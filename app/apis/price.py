from flask import request, jsonify, make_response
from flask_restplus import Resource, Namespace, reqparse, fields

from app.core.api_helpers.price import price_predict


api = Namespace('get_price', description='Message operations', path='/v1')

parser_args = api.parser()
parser_args.add_argument('district', type=str, location='form')
parser_args.add_argument('square', type=str, location='form')
parser_args.add_argument('rooms', type=str, location='form')

auth_model = api.model('Auth', {
    'district': fields.String(description='Application ID'),
    'square': fields.String(description='Application Password'),
    'rooms': fields.String(description='Application Password')
})


@api.route('/get_price')
class Content(Resource):
    """
    Endpoint for predicting message body and subject using pre trained classifier
    """

    @api.doc('simple message body detection')
    @api.expect(parser_args)
    def post(self):
        """Simple api check"""
        args = parser_args.parse_args()
        if request.get_json():
            args['district'] = args['district'] or request.get_json().get(
                'district', None)
            args['square'] = args['square'] or request.get_json().get(
                'square', None)
            args['rooms'] = args['rooms'] or request.get_json().get(
                'rooms', None)

        res = price_predict(**args)
        res = make_response(
            jsonify(res), 200
        )
        res.headers['content-type'] = 'application/json'
        return res
