from flask_restplus import Api

from .price import api as price_api


api = Api(
    title='Flat Price API',
    description='Flat price API '
                'Use this API to predict price for your flat. '
                'This service use pretrained model for prices.',
    prefix='/api',
    doc='/doc',
    # authorizations=authorizations,
    validate=True
)

api.add_namespace(price_api)
