import os

import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report, log_loss

from app.settings import project_config


def start():
    dataset = pd.read_csv(
        os.path.join(project_config.APP_DIR, 'flat', 'parse_result.csv'))

    print(dataset.head())
    print(dataset.describe())
    X_train, X_test, y_train, y_test = train_test_split(
        dataset[dataset.columns[0]],
        dataset[dataset.columns[1]],
        test_size=0.3,
        random_state=256,
        # stratify=dataset[dataset.columns[1]])
    )


if __name__ == '__main__':
    start()
