__all__ = [
    'NLPServiceError',
    'JobIDNotFoundError',
    'FileExtensionError',
]


class NLPServiceError(Exception):
    pass


class JobIDNotFoundError(NLPServiceError):
    pass


class FileExtensionError(NLPServiceError):
    pass
