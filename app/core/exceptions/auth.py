__all__ = [
    'AuthorizationError',
    'PasswordMismatchError',
    'TokenNotFoundError'
]


class AuthorizationError(Exception):
    pass


class PasswordMismatchError(AuthorizationError):
    pass


class TokenNotFoundError(AuthorizationError):
    pass
