import pandas as pd
import xgboost as xgb

from app import (mean_rooms_disctricts, mean_square_disctricts,
                 median_rooms_disctricts, median_square_disctricts,
                 avg_price_per_square_disctricts,
                 avg_price_per_room_disctricts, districts_dict, districts,
                 xgb_model, df)


def df_preprocessing(df, onehot=False):
    df['dev_mean_square_district'] = df.apply(
        lambda x: x['square'] - mean_square_disctricts.get(x['district']),
        axis=1)
    df['dev_median_square_district'] = df.apply(
        lambda x: x['square'] - median_square_disctricts.get(x['district']),
        axis=1)

    df['dev_mean_rooms_district'] = df.apply(
        lambda x: x['rooms'] - mean_rooms_disctricts.get(x['district']),
        axis=1)
    df['dev_median_rooms_district'] = df.apply(
        lambda x: x['rooms'] - median_rooms_disctricts.get(x['district']),
        axis=1)

    df['avg_price_per_room_disctricts'] = df.apply(
        lambda x: avg_price_per_room_disctricts.get(x['district']),
        axis=1)

    df['avg_price_per_square_disctricts'] = df.apply(
        lambda x: avg_price_per_square_disctricts.get(x['district']),
        axis=1)

    df['avg_room_size'] = df.apply(lambda x: x['square'] / x['rooms'], axis=1)

    if onehot:
        df = pd.get_dummies(df, columns=['district'])
    return df


def data_format_for_predicting(data: dict):
    data.update(districts_dict)
    district = [s for s in districts if data['district'] in s][0]
    data[district] = 1

    df = pd.DataFrame([data])
    df = df_preprocessing(df)
    df = df.drop(labels=['district'], axis=1)

    return df


new_df = df_preprocessing(df, onehot=True)
X, y = new_df.drop(
    labels=['price', 'id', 'date', 'price/square', 'price/rooms'], axis=1), \
       new_df[['price']]

columns_order = X.columns


def price_predict(district, square, rooms, **kwargs):
    error, msg = False, 'Recognized'
    status_code = 200

    try:
        data = {
            'district': district.replace('\\', ''),
            'square': float(square),
            'rooms': float(rooms)
        }
        # data = {
        #     'district': "Дарницький",
        #     'square': 200,
        #     "rooms": 3
        # }

        x = data_format_for_predicting(data)[columns_order]
        test_sample = xgb.DMatrix(x)
        pred_value = xgb_model.predict(test_sample)[0]

    except Exception as e:
        error, msg = True, 'Server error'
        status_code = 500
        raise e

    return {
        'error': error,
        'msg': msg,
        'result': int(pred_value),
        'status_code': status_code
    }
