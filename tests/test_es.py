import requests

from app import app


def test_gel_all():
    url = '{}/test_flat/post/_search'.format(app.config['ES_URL'])

    r = requests.get(url)
    res = r.json()
    print(r)


def test_one_flat():
    res = {
              "_index": "test_flat",
              "_type": "flat",
              "_id": "344719917",
              "_score": 1,
              "_source": {
                  "rooms": 2,
                  "items": [
                      "2-кімнатна",
                      "64 м²",
                      "КТУ",
                      "2006 р",
                      "монолітно-каркасний"
                  ],
                  "district": [
                      "Дарницький",
                      "Позняки"
                  ],
                  "price": 13500,
                  "square": 64,
                  "price/square":""
              }
          },

# медиану найти, по квартирам
# найти этаж
# Комната, квадратура,цена, этаж, год, локация
# сохранять в csv
