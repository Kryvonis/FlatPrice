import requests
import json

from elasticsearch import Elasticsearch


es_cli = Elasticsearch(['localhost:9200'])

ES_URL = 'http://127.0.0.1:9200'
ES_INDEX = 'test_flat'
ES_TYPE = 'flat'


def save_flat(flat):
    _id = flat.pop('id')
    if not es_cli.exists(index=ES_INDEX, doc_type=ES_TYPE, id=_id):
        url = '{base}/{index}/{type}/{id}/'.format(
            base=ES_URL,
            index=ES_INDEX,
            type=ES_TYPE,
            id=_id,
        )
        res = requests.post(url, data=json.dumps(flat))


def get_all_flats():
    from_ = 0
    es_q = {
        "query": {
            "match_all": {}
        }
    }
    res = []
    total_count = 1
    while total_count > 0:
        search = es_cli.search(index=ES_INDEX, doc_type=ES_TYPE, body=es_q,
                               size=1000, from_=from_)
        total_count = len(search['hits']['hits'])
        res.extend(search['hits']['hits'])
        from_ += 1000

    return res
