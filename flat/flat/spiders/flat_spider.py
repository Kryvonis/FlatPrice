import scrapy
import csv
from datetime import datetime
from scrapy.selector import HtmlXPathSelector

from flat.db import crud


DOLLAR_COURSE = 28
EURO_COURSE = 31


class BlogSpider(scrapy.Spider):
    name = 'flat'

    start_urls = [
        'https://www.lun.ua/uk/%D0%BE%D1%80%D0%B5%D0%BD%D0%B4%D0%B0-%D0%BA%D0%B2%D0%B0%D1%80%D1%82%D0%B8%D1%80-%D0%BA%D0%B8%D1%97%D0%B2?page={page}'.format(
            page=p)
        for p in range(30)]

    def _make_card_json(self, card):
        valid = False
        _id = card.xpath(
            "./@data-jss").extract()[0].split('-')[-1]
        price = card.xpath(
            ".//div[contains(@class, 'realty-card-characteristics__price')]/text()").extract()[
            0]
        if price.endswith('грн'):
            price = int(''.join(price[:-4].split()))
        elif price.endswith('$'):
            price = int(''.join(price[:-1].split())) * DOLLAR_COURSE
        elif price.endswith('€'):
            price = int(''.join(price[:-1].split())) * EURO_COURSE
        else:
            return False, None
        districts = card.xpath(
            ".//p[contains(@class, 'realty-card-header__subtitle')]/a/text()").extract()
        if districts:
            if 'ЖК' in districts[0]:
                return False, None
            main_district = districts[0]
        else:
            return False, None
        items = card.xpath(
            ".//div[contains(@class, 'realty-card-characteristics-list__item')]/text()").extract()
        square = 0
        rooms = 0
        try:
            for item in items:
                if 'м²' in item:
                    square = float(item.split()[0])
                elif '-кімнатна' in item:
                    rooms = int(item.split('-кімнатна')[0])
        except Exception:
            return False, None

        if not (rooms and square):
            return False, None
        return True, {
            'id': _id,
            'price': price,
            'district': districts,
            'items': items,
            'square': square,
            'rooms': rooms,
            'date': datetime.strftime(datetime.utcnow(), '%Y-%m-%dT%H:%M:%SZ'),
            'price/square': price/square,
            'price/rooms': price/rooms
        }

    def parse(self, response):
        PAGE_LIST_SELECTOR = "//div[contains(concat(' ', @class, ' '), ' realty-card ')]"
        cards = response.xpath(PAGE_LIST_SELECTOR)
        for i, card in enumerate(cards):
            # hxs = HtmlXPathSelector(card)
            valid, card_json = self._make_card_json(card)
            if valid:
                crud.save_flat(card_json)
            else:
                print(card)

    def closed(self, reason):
        # will be called when the crawler process ends
        # any code
        # do something with collected data
        print('Transform db to csv')
        with open('parse_result.csv', 'w') as f:
            csv_file = csv.writer(f)
            csv_file.writerow(['id','price', 'district', 'square', 'rooms', 'date', 'price/square', 'price/rooms'])
            try:
                for flat in list(crud.get_all_flats()):
                    csv_file.writerow([
                        flat['_id'],
                        flat['_source']['price'],
                        flat['_source']['district'][0],
                        flat['_source']['square'],
                        flat['_source']['rooms'],
                        flat['_source']['date'],
                        flat['_source']['price/square'],
                        flat['_source']['price/rooms'],

                    ])
            except Exception as e:
                print(flat['_source'])
                raise e
